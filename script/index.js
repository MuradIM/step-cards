import createHeader from "./functions/header/createHeader.js";
import board from "./classes/board/board.js";
import logModal from "./classes/modal/LogModal.js";
import LogButton from "./classes/btn/logButton.js";
import LogButtonSubmit from "./classes/btn/logButtonSubmit.js";
import replaceLogButton from "./functions/header/replaceLogButton.js";
import makeAllCardsRequest from "./functions/requests/makeAllCardsRequest.js";
import createFilters from "./functions/body/filters/createFilters.js";

createHeader()
board.createBoard()
logModal.renderModal()

const logBtn = new LogButton(document.querySelector(".js-log-btn"))
const logBtnSubmit = new LogButtonSubmit(document.querySelector(".js-log-btn-submit"))

logBtn.addAction()
logBtnSubmit.addAction()

if (sessionStorage.getItem("token")) {
    replaceLogButton()
    createFilters()
    makeAllCardsRequest()
}