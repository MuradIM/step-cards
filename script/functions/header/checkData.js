import logModal from "../../classes/modal/LogModal.js";

async function checkData(e) {
    e.preventDefault()
    return await logModal.userVerification()
}

export default checkData