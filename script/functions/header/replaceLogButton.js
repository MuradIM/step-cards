import CreateVisitButton from "../../classes/btn/createVisitButton.js";

function replaceLogButton() {
    const createVisitButton = new CreateVisitButton()
    const logButton = document.querySelector(".js-log-btn")
    const buttonPlace = document.querySelector(".js-log-btn-wrapper")

    logButton.remove()
    buttonPlace.append(createVisitButton.renderButton())
}

export default replaceLogButton