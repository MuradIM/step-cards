import createVisitModal from "../../classes/modal/createVisitModal.js";

function showCreateVisitModal(e) {
    e.stopPropagation()
    createVisitModal.showModal()
}

export default showCreateVisitModal