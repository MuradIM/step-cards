import createCard from "../body/submitVisitForm/createCard.js";

function makeAllCardsRequest() {
    const token = sessionStorage.getItem("token")

    return fetch("https://ajax.test-danit.com/api/v2/cards", {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
    })
        .then(response => response.json())
        .then(response => {
            response.forEach(e => {
                createCard(e)
            })
        })
}

export default makeAllCardsRequest