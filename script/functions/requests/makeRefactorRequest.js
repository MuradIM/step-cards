function makeRefactorRequest(obj) {
    const token = sessionStorage.getItem("token")

    return fetch(`https://ajax.test-danit.com/api/v2/cards/${obj.id}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify({...obj})
    })
        .then(response => response.json())
        .then(response => {
            return response
        })
}

export default makeRefactorRequest