function makeRemoveRequest(id) {
    const token = sessionStorage.getItem("token")

   return fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
        method: 'DELETE',
        headers: {
            'Authorization': `Bearer ${token}`
        },
    })
}

export default makeRemoveRequest