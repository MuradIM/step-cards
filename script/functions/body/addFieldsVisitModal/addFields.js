import mutualAddHTML from "../../../elements/board/createVisit/mutualAddHTML.js";
import cardiologistModalSpecial from "../../../elements/board/createVisit/cardiologistModalSpecial.js";
import dentistModalSpecial from "../../../elements/board/createVisit/dentistModalSpecial.js";
import therapistModalSpecial from "../../../elements/board/createVisit/therapistModalSpecial.js";

function addFieldsF(e) {
    const place = document.querySelector(".js-choose-doctor-input")
    const placeSpecial = document.querySelector(".js-choose-doctor-special-input")
    if (e.target.value !== "") {
        place.innerHTML = mutualAddHTML
    } else {
        place.innerHTML = ""
        placeSpecial.innerHTML = ""
    }

    if (e.target.value === "cardiologist") {
        placeSpecial.innerHTML = cardiologistModalSpecial
    } else if (e.target.value === "dentist") {
        placeSpecial.innerHTML = dentistModalSpecial
    } else if (e.target.value === "therapist") {
        placeSpecial.innerHTML = therapistModalSpecial
    }
}

export default addFieldsF