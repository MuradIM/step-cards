import createObjectToRequest from "../submitVisitForm/createObjectToRequest.js";
import makeRefactorRequest from "../../requests/makeRefactorRequest.js";
import createCard from "../submitVisitForm/createCard.js";

function refactorCreateVisit(e, id) {

    e.preventDefault()
    const objectToRequest = createObjectToRequest(e.target)
    const status = e.target.querySelector(".js-status")

    objectToRequest.id = id
    objectToRequest.isDone = status.hasAttribute("checked")

    return makeRefactorRequest(objectToRequest)
        .then(responseObj => {
            const thisCardBeforeUpdate = document.querySelector(`#cardNum${responseObj.id}`)
            thisCardBeforeUpdate.remove()

            return responseObj
        })
        .then(responseObj => createCard(responseObj))
}

export default refactorCreateVisit