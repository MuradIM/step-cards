function createNav() {
    const nav = document.createElement("nav")
    nav.classList.add("navbar", "navbar-light", "bg-light", "container")
    return nav
}

export default createNav