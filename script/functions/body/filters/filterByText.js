function filterByText(requestText, i) {
    const nameInfo = i.querySelector(".js-name-for-search").textContent.toLowerCase()
    const shortDescription = i.querySelector(".js-short-description-for-search").textContent.toLowerCase()
    const visitGoal = i.querySelector(".js-goal-for-search").textContent.toLowerCase()

    i.classList.add("d-none")

    if (nameInfo.includes(requestText) || shortDescription.includes(requestText) || visitGoal.includes(requestText)) {
        i.classList.remove("d-none")
    }
}

export default filterByText