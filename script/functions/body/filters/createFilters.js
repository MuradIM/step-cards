import createNav from "./createNav.js";
import FilterForm from "../../../classes/form/filterForm.js";

function createFilters() {
    const nav = createNav()
    const container = document.querySelector(".js-container")
    const wrapper = document.createElement("div")
    const form = new FilterForm()

    wrapper.classList.add("container-fluid")

    wrapper.append(form.render())
    nav.append(wrapper)
    container.insertAdjacentElement("beforebegin", nav)
}

export default createFilters