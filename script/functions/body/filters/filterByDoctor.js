function filterByDoctor(doctorChosen, i) {
    let doctor = i.querySelector(".js-doctor-for-search").textContent

    if (doctor === "Кардиолог") {
        doctor = "cardiologist"
    } else if (doctor === "Стоматолог") {
        doctor = "dentist"
    } else if (doctor === "Терапевт") {
        doctor = "therapist"
    }

    if (doctorChosen !== doctor) {
        i.classList.add("d-none")
    }
}

export default filterByDoctor