function filterByStatus(statusChosen, i) {
    const status = i.querySelector(".js-status")

    if (statusChosen === "active" && status.classList.contains("js-inactive")) {
        i.classList.add("d-none")
    } else if (statusChosen === "inactive" && status.classList.contains("js-active")) {
        i.classList.add("d-none")
    }
}

export default filterByStatus