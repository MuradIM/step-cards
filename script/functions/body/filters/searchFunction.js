import filterByStatus from "./filterByStatus.js";
import filterByDoctor from "./filterByDoctor.js";
import filterByText from "./filterByText.js";

function searchFunction ({status, doctor, requestText}) {
    const container = document.querySelector(".js-card-board")
    const cards = Array.from(document.querySelectorAll(".js-card"))

    const warningMessage = document.createElement("p")
    warningMessage.classList.add("js-warning-message")
    warningMessage.textContent = "Ничего не найдено"

        cards.forEach(i => {

            i.classList.remove("d-none")

            if (requestText) {
                filterByText(requestText.toLowerCase(), i)
            }

            if (status) {
               filterByStatus(status, i)
            }

            if (doctor) {
                filterByDoctor(doctor, i)
            }

        })

    const cardsHidden = document.querySelectorAll(".d-none.js-card")
    const message = document.querySelector(".js-warning-message")

    if (cardsHidden.length === cards.length) {
        if (!message) {
            container.append(warningMessage)
        }
    } else {
        if (message) {
            message.remove()
        }
    }
}

export default searchFunction