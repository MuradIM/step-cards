function addToPage(HTML, cardId) {
    const grid = document.querySelector(".js-card-board")


    const div = document.createElement("div")
    div.classList.add("col", "js-card")
    div.id = `cardNum${cardId}`
    div.innerHTML = HTML

    if (grid.children.length === 0) {
        const p = document.querySelector(".js-initial-text")
        if (p) {
            p.remove()
        }
    }

    grid.append(div)
}

export default addToPage