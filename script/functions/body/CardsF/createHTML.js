import createAdditionalInfo from "./createAdditionalInfo.js";

function createHTML(obj) {
    let doctor

    switch (obj.doctor) {
        case "cardiologist":
            doctor = "Кардиолог"
            break
        case "dentist":
            doctor = "Стоматолог"
            break
        case "therapist":
            doctor = "Терапевт"
            break
    }

    let cardColorStyles

    if (obj.isDone === true) {
        cardColorStyles = "card text-dark bg-light js-status js-inactive"
    } else {
        if (obj.priority === "usualPriority") {
            cardColorStyles = "card text-white bg-success js-status js-active"
        } else if (obj.priority === "highPriority") {
            cardColorStyles = "card text-dark bg-warning js-status js-active"
        } else if (obj.priority === "urgentPriority") {
            cardColorStyles = "card text-white bg-danger js-status js-active"
        }
    }



    return `
     <div class="${cardColorStyles}">
      <div class="card-body">
        <div class="js-close-btn-wrapper"></div>
        <h5 class="card-title js-doctor-for-search">${doctor}</h5>
        <p class="card-text js-name-for-search">${obj.nameInfo}</p>
        <div class="js-more-info d-none">${createAdditionalInfo(obj)}</div>
        <div class="btn-group pt-3 js-button-group-wrapper" role="group" aria-label="Basic mixed styles example">
        </div>
      </div>
    </div>
    `
}

export default createHTML