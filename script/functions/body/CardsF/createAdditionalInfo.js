import cardiologistHTML from "../../../elements/board/cardsHTML/cardiologistHTML.js";
import dentistHTML from "../../../elements/board/cardsHTML/dentistHTML.js";
import therapistHTML from "../../../elements/board/cardsHTML/therapistHTML.js";

function createAdditionalInfo(obj) {

    let HTML
    let priority

    if (obj.priority === "usualPriority") {
        priority = "обычная"
    } else if (obj.priority === "highPriority") {
        priority = "высокая"
    } else if (obj.priority === "urgentPriority") {
        priority = "неотложная"
    }

    switch (obj.doctor) {
        case "cardiologist":
            HTML = cardiologistHTML(obj, priority)
            break
        case "dentist":
            HTML = dentistHTML(obj, priority)
            break
        case "therapist":
            HTML = therapistHTML(obj, priority)
            break
    }

    return HTML
}

export default createAdditionalInfo