import getInfoFromInputs from "./getInfoFromInputs.js";
import getInfoFromSelects from "./getInfoFromSelects.js";

function createObjectToRequest(element) {
    let inputArr = Array.from(element.querySelectorAll(".js-info-for-object"))
    let selectArr = Array.from(element.querySelectorAll("select"))

    const infoFromInputs = inputArr.map(getInfoFromInputs)
    const infoFromSelects = selectArr.map(getInfoFromSelects)
    const infoArray = [...infoFromSelects, ...infoFromInputs]
    const infoObject = Object.assign({}, infoArray)

    const objectToRequest = {}

    for (let i = 0; i < Object.keys(infoObject).length; i++) {
        const arr = infoObject[i]
        const key = Object.keys(arr)
        objectToRequest[key] = arr[key]
    }

    return objectToRequest
}

export default createObjectToRequest