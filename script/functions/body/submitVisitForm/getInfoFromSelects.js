function getInfoFromSelects(arr) {
    if (arr.value === "cardiologist" || arr.value === "dentist" || arr.value === "therapist") {
        return {doctor: arr.value}
    }
    if (arr.value === "usualPriority" || arr.value === "highPriority" || arr.value === "urgentPriority") {
        return {priority: arr.value}
    }
}

export default getInfoFromSelects