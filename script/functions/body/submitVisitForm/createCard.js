import VisitCardiologist from "../../../classes/visit/VisitCardiologist.js";
import VisitDentist from "../../../classes/visit/VisitDentist.js";
import VisitTherapist from "../../../classes/visit/VisitTherapist.js";

function createCard(obj) {
    let card

    switch (obj.doctor) {
        case "cardiologist":
            card = new VisitCardiologist(obj)
            card.renderCard()
            break
        case "dentist":
            card = new VisitDentist(obj)
            card.renderCard()
            break
        case "therapist":
            card = new VisitTherapist(obj)
            card.renderCard()
            break
    }
}

export default createCard