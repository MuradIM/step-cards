import createObjectToRequest from "./createObjectToRequest.js";
import makeAddRequest from "../../requests/makeAddRequest.js";
import createCard from "./createCard.js";
import createVisitModal from "../../../classes/modal/createVisitModal.js";

function submitCreateVisit(e) {
    e.preventDefault()
    const objectToRequest = createObjectToRequest(e.target)
    objectToRequest.isDone = false
    makeAddRequest(objectToRequest)
        .then(responseObj => createCard(responseObj))
        .then(() => createVisitModal.closeModal())
}

export default submitCreateVisit