class DoctorSelect {
    constructor() {
    }

    render() {
        this.select = document.createElement("select")
        this.select.classList.add("form-select", "js-doctor-search")

        const placeholder = new Option("Выберете доктора", "0")
        const cardiologist = new Option("Кардиолог", "cardiologist")
        const dentist = new Option("Стоматолог", "dentist")
        const therapist = new Option("Терапевт", "therapist")

        this.select.append(placeholder)
        this.select.append(cardiologist)
        this.select.append(dentist)
        this.select.append(therapist)

        return this.select
    }

}

export default DoctorSelect