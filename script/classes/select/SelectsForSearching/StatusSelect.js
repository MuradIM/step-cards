class StatusSelect {
    constructor() {
    }

    render() {
        this.select = document.createElement("select")
        this.select.classList.add("form-select", "js-status-search")

        const placeholder = new Option("Укажите статус", "0")
        const active = new Option("Визит еще не прошел", "active")
        const inactive = new Option("Визит уже состоялся", "inactive")

        this.select.append(placeholder)
        this.select.append(active)
        this.select.append(inactive)

        return this.select
    }
}

export default StatusSelect