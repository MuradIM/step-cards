import SearchInput from "../input/SearchInput.js";
import DoctorSelect from "../select/SelectsForSearching/DoctorSelect.js";
import StatusSelect from "../select/SelectsForSearching/StatusSelect.js";
import SubmitSearchButton from "../btn/submitSearchButton.js";
import searchFunction from "../../functions/body/filters/searchFunction.js";

class FilterForm {
    constructor() {
    }

    render() {
        this.form = document.createElement("form")

        const searchInput = new SearchInput()
        const doctorSelect = new DoctorSelect()
        const statusSelect = new StatusSelect()
        const submitButton = new SubmitSearchButton()

        this.form.append(searchInput.render())
        this.form.append(doctorSelect.render())
        this.form.append(statusSelect.render())
        this.form.append(submitButton.render())

        this.form.classList.add("d-flex", "input-group")

        this.form.addEventListener("submit", e => {
            this.searchEvent(e)
        })

        return this.form
    }

    searchEvent(e) {
        e.preventDefault()
        let searchValues = {}
        this.form.childNodes.forEach(i => {
            if (i.tagName === "INPUT" && i.value.trim() !== "") {
                searchValues.requestText = i.value
            } else if (i.tagName === "SELECT" && i.value !== "0") {
                    if (i.classList.contains("js-doctor-search")) {
                        searchValues.doctor = i.value
                    } else if (i.classList.contains("js-status-search")) {
                        searchValues.status = i.value
                    }
            }
        })
        if (searchValues.length !== 0) {
            searchFunction(searchValues)
        }
    }
}

export default FilterForm