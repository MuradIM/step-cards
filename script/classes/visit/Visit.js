import createHTML from "../../functions/body/CardsF/createHTML.js";
import addToPage from "../../functions/body/CardsF/addToPage.js";
import RemoveButton from "../btn/visitCardButtons/removeButton.js";
import RefactorButton from "../btn/visitCardButtons/refactorButton.js";
import ShowMoreButton from "../btn/visitCardButtons/showMoreButton.js";


class Visit {
    constructor({visitGoal, shortDescription, id, priority, nameInfo, isDone}) {
        this.visitGoal = visitGoal
        this.shortDescription = shortDescription
        this.id = id
        this.priority = priority
        this.nameInfo = nameInfo
        this.isDone = isDone
    }

    renderCard() {
        const HTML = createHTML(this)
        addToPage(HTML, this.id)

        this.removeBtn = new RemoveButton(document.createElement("button"), this)
        this.refactorBtn = new RefactorButton(document.createElement("button"), this)
        this.showMoreBtn = new ShowMoreButton(document.createElement("button"), this.id)
        const removeBtnWrapper = document.querySelector(`#cardNum${this.id}`).querySelector(".js-close-btn-wrapper")
        const groupBtnWrapper = document.querySelector(`#cardNum${this.id}`).querySelector(".js-button-group-wrapper")

        removeBtnWrapper.append(this.removeBtn.renderBtn())
        groupBtnWrapper.append(this.showMoreBtn.renderBtn(), this.refactorBtn.renderBtn())
    }
}

export default Visit