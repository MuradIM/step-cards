import Visit from "./Visit.js";

class VisitTherapist extends Visit {
    constructor(obj) {
        super(obj)
        this.doctor = obj.doctor
        this.age = obj.age
    }
}

export default VisitTherapist