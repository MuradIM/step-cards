import Visit from "./Visit.js";

class VisitCardiologist extends Visit {
    constructor(obj) {
        super(obj)
        this.doctor = obj.doctor
        this.pressure = obj.pressure
        this.BMI = obj.BMI
        this.pastIllness = obj.pastIllness
        this.age = obj.age
    }
}

export default VisitCardiologist