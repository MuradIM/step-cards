import Visit from "./Visit.js";


class VisitDentist extends Visit {
    constructor(obj) {
        super(obj)
        this.doctor = obj.doctor
        this.lastVisitDate = obj.lastVisitDate
    }
}

export default VisitDentist