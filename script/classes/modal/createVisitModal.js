import createVisitHTML from "../../elements/board/createVisit/createVisitHTML.js";
import addFieldsF from "../../functions/body/addFieldsVisitModal/addFields.js";
import submitCreateVisit from "../../functions/body/submitVisitForm/submitCreateVisit.js";

class CreateVisitModal {
    constructor() {

    }

    renderModal() {
        this.modal = document.createElement("div")
        this.modal.classList.add("modal", "fade", "js-modal", "show")
        this.modal.innerHTML = createVisitHTML
        this.modal.style.display = "block"

        return this.modal
    }

    renderBackground() {
        this.modalBackground = document.createElement("div")
        this.modalBackground.classList.add("modal-backdrop", "fade", "show")
        this.modal.style.display = "block"

        return this.modalBackground
    }

    showModal() {
        this.renderModal()
        this.renderBackground()
        document.body.append(this.modal)
        document.body.append(this.modalBackground)
        this.addFields()
        this.submitForm()

        this.modal.addEventListener("mousedown", e => {
            if (!e.target.closest(".modal-content") || e.target.classList.contains("btn-close")) {
                this.closeModal(e)
            }
        })
    }

    closeModal() {
        this.modal.remove()
        this.modalBackground.remove()
    }

    addFields() {
        const options = document.querySelector(".js-choose-doctor-select")
        options.addEventListener("click", addFieldsF)
    }

    submitForm() {
        this.form = document.querySelector(".js-create-visit-modal-form")
        this.form.addEventListener("submit", submitCreateVisit)
    }
}

const createVisitModal = new CreateVisitModal()

export default createVisitModal