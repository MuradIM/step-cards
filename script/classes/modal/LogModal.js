import logModalHTML from "../../elements/header/logModalHTML.js";
import stylesAdd from "../../functions/header/addStylesLogModal.js";
import stylesRemove from "../../functions/header/removeStylesLogModal.js";
import checkUserData from "../../functions/header/checkUserData.js";
import replaceLogButton from "../../functions/header/replaceLogButton.js";
import makeAllCardsRequest from "../../functions/requests/makeAllCardsRequest.js";
import createFilters from "../../functions/body/filters/createFilters.js";

class LogModal {
    constructor() {

    }

    renderModal() {
        this.modal = document.createElement("div")
        this.modal.classList.add("modal", "fade", "js-modal")
        this.modal.innerHTML = logModalHTML
        this.renderBackground()
        document.body.append(this.modal)

        return this.modal
    }

    renderBackground() {
        this.modalBackground = document.createElement("div")
        this.modalBackground.classList.add("modal-backdrop", "fade")
        this.modalBackground.style.display = "none"
        document.body.append(this.modalBackground)

        return this.modalBackground
    }

    showModal() {
        stylesAdd(this.modal)
        stylesAdd(this.modalBackground)

        this.modal.addEventListener("mousedown", e=> {
            if (!e.target.closest(".modal-content") || e.target.classList.contains("btn-close")) {
                this.closeModal(e)
            }
        })
    }

    closeModal() {
        stylesRemove(this.modal)
        stylesRemove(this.modalBackground)
    }

    async userVerification() {
        const emailToAccess = document.querySelector(".js-email")
        const passwordToAccess = document.querySelector(".js-password")

        this.tokenRequest = await checkUserData(emailToAccess.value, passwordToAccess.value)

        if (this.tokenRequest.ok) {
            this.token = await this.tokenRequest.text()

            sessionStorage.setItem("token", `${this.token}`)

            this.closeModal()
            replaceLogButton()
            createFilters()
            makeAllCardsRequest()
        } else {
            alert(`Incorrect username or password`)
        }
    }
}

const logModal = new LogModal()

export default logModal