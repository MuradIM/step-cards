import refactorVisitModal from "../../elements/board/refactorVisit/refactorVisitModal.js";
import refactorCreateVisit from "../../functions/body/refactorModalF/refactorCreateVisit.js";

class RefactorCardModal {
    constructor(obj) {
        this.obj = obj
        this.id = obj.id
    }

    renderModal() {
        this.modal = document.createElement("div")
        this.modal.classList.add("modal", "fade", "js-modal", "show")
        this.modal.append(refactorVisitModal(this.obj))
        this.modal.style.display = "block"

        return this.modal
    }

    renderBackground() {
        this.modalBackground = document.createElement("div")
        this.modalBackground.classList.add("modal-backdrop", "fade", "show")
        this.modal.style.display = "block"

        return this.modalBackground
    }

    showModal() {
        this.renderModal()
        this.renderBackground()
        document.body.append(this.modal)
        document.body.append(this.modalBackground)
        this.submitForm()

        this.modal.addEventListener("mousedown", e => {
            if (!e.target.closest(".modal-content") || e.target.classList.contains("btn-close")) {
                this.closeModal(e)
            }
        })
    }

    closeModal() {
        this.modal.remove()
        this.modalBackground.remove()
    }

    submitForm() {
        const form = document.querySelector(".js-create-visit-modal-form")

        form.addEventListener("submit", e => {
            refactorCreateVisit(e, this.id)
                .then(() => {
                    this.closeModal()
                })
        })
    }

}

export default RefactorCardModal