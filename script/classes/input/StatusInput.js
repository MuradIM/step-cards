class StatusInput {
    constructor(isDone) {
        this.isDone = isDone
    }

    render() {
        this.input = document.createElement("input")
        this.input.classList.add("form-check-input", "js-status")
        this.input.setAttribute("type", "checkbox")
        this.input.setAttribute("role", "switch")
        this.input.setAttribute("data-bs-toggle", "tooltip")
        this.input.setAttribute("data-bs-placement", "right")
        this.input.setAttribute("title", "Визит еще не состоялся")

        if (this.isDone) {
            this.input.setAttribute("checked", "true")
            this.input.setAttribute("title", "Визит состоялся")
        }

        this.input.addEventListener("click", () => {
            this.input.toggleAttribute("checked")
        })

        return this.input
    }


}

export default StatusInput