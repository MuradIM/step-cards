class SearchInput {
    constructor() {
    }

    render() {
        this.input = document.createElement("input")
        this.input.classList.add("form-control", "me-2", "w-25")
        this.input.setAttribute("type", "search")
        this.input.setAttribute("placeholder", "Поиск по имени, описанию или цели визита")
        this.input.setAttribute("aria-label", "Search")

        return this.input
    }
}

export default SearchInput