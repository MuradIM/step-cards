class SubmitSearchButton {
    constructor() {
    }

    render () {
        this.btn = document.createElement("button")
        this.btn.classList.add("btn", "btn-outline-success")
        this.btn.setAttribute("type", "submit")
        this.btn.textContent = "Поиск"

        return this.btn
    }
}

export default SubmitSearchButton