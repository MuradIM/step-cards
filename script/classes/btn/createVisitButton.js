import showCreateVisitModal from "../../functions/header/showCreateVisitModal.js";

class CreateVisitButton {
    constructor() {

    }

    renderButton() {
        this.btn = document.createElement("button")
        this.btn.classList.add("btn", "btn-outline-light", "me-2", "js-create-visit-btn")
        this.btn.textContent = "Создать визит"
        this.btn.addEventListener("click", showCreateVisitModal)

        return this.btn
    }
}

export default CreateVisitButton