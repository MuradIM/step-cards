import Button from "../Button.js";
import ShowMoreButton from "./showMoreButton.js";

class ShowLessButton extends Button {
    constructor(btn, id) {
        super(btn);
        this.id = id
    }

    renderBtn() {
        this.btn.classList.add("btn", "btn-primary", "js-btn-show-less")
        this.btn.textContent = "Скрыть доп. инфо."
        this.btn.addEventListener("click", () => {
            this.showLess()
        })

        return this.btn
    }

    showLess() {
        this.card = document.querySelector(`#cardNum${this.id}`)
        this.infoWrapper = this.card.querySelector(".js-more-info")
        this.button = this.card.querySelector(".js-btn-show-less")
        this.infoWrapper.classList.add("d-none")
        this.button.remove()

        this.btnWrapper = this.card.querySelector(".js-button-group-wrapper")
        this.showMoreBtn = new ShowMoreButton(document.createElement("button"), this.id)
        this.btnWrapper.prepend(this.showMoreBtn.renderBtn())
    }
}

export default ShowLessButton