import Button from "../Button.js";
import RefactorCardModal from "../../modal/refactorCardModal.js";

class RefactorButton extends Button {
    constructor(btn, context) {
        super(btn, context);
        this.context = context
    }

    renderBtn() {
        this.btn.classList.add("btn", "btn-secondary", "js-btn-refactor-card")
        this.btn.textContent = "Редактировать"
        this.btn.addEventListener("click", () => {
            this.refactorCard()
        })

        return this.btn
    }

    refactorCard() {
        this.refactorModal = new RefactorCardModal(this.context)
        this.refactorModal.showModal()
    }
}

export default RefactorButton