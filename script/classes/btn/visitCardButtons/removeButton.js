import Button from "../Button.js";
import makeRemoveRequest from "../../../functions/requests/makeRemoveRequest.js";

class RemoveButton extends Button {
    constructor(btn, context) {
        super(btn);
        this.context = context
    }

    renderBtn() {
        this.btn.classList.add("btn-close", "position-absolute", "top-0", "end-0", "mt-2", "me-2")
        this.btn.addEventListener("click", () => {
            confirm("Вы действительно хотите удалить карточку?") ? this.removeCard() : null
        })
        return this.btn
    }

    removeCard() {
        makeRemoveRequest(this.context.id)
            .then(() => {
                const grid = document.querySelector(".js-card-board")
                document.querySelector(`#cardNum${this.context.id}`).remove()
                    if (grid.children.length === 0) {
                        const container = document.querySelector(".js-container")
                        const initialText = document.createElement("p")
                        initialText.classList.add("js-initial-text")
                        initialText.textContent = `No items have been added`
                        container.prepend(initialText)
                    }
            })
    }
}

export default RemoveButton