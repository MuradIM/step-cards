import Button from "../Button.js";
import ShowLessButton from "./showLessButton.js";

class ShowMoreButton extends Button {
    constructor(btn, id) {
        super(btn);
        this.id = id
    }

    renderBtn() {
        this.btn.classList.add("btn", "btn-primary", "js-btn-show-more")
        this.btn.textContent = "Показать больше"
        this.btn.addEventListener("click", () => {
            this.showMore()
        })

        return this.btn
    }

    showMore() {
        const card = document.querySelector(`#cardNum${this.id}`)
        const infoWrapper = card.querySelector(".js-more-info")
        const button = card.querySelector(".js-btn-show-more")
        infoWrapper.classList.remove("d-none")

        button.remove()

        const btnWrapper = card.querySelector(".js-button-group-wrapper")
        this.showLessBtn = new ShowLessButton(document.createElement("button"), this.id)
        btnWrapper.prepend(this.showLessBtn.renderBtn())

    }


}

export default ShowMoreButton