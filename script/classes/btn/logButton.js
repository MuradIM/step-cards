import Button from "./Button.js";
import logBtnF from "../../functions/header/logBtnF.js";

class LogButton extends Button {
    constructor(btn) {
        super(btn);
    }

    addAction() {
        this.btn.addEventListener("click", (e) => {
            logBtnF(e)
        })
    }
}

export default LogButton