import container from "../../elements/board/container.js";

class Board {
    constructor() {
    }

    createText() {
        this.initialText = document.createElement("p")
        this.initialText.classList.add("js-initial-text")
        this.initialText.textContent = `No items have been added`
        container.append(this.initialText)
    }

    createBoard() {
        this.createText()

        const cardsContainer = document.createElement("div")

        cardsContainer.classList.add("row", "row-cols-1", "row-cols-md-3", "g-4", "js-card-board")
        container.append(cardsContainer)
        document.body.appendChild(container)
    }
}

const board = new Board()

export default board