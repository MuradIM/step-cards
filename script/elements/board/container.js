const container = document.createElement("div")

container.classList.add("container-lg", "border", "border-dark", "rounded-3", "mt-2", "width-vh", "pt-3", "js-container")

export default container