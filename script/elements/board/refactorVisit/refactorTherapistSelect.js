const refactorTherapistSelect = `
<select class="form-select form-select-default js-choose-doctor-select" aria-label=".form-select-default example" required="required">
    <option value="cardiologist">Кардиолог</option>
    <option value="dentist">Стоматолог</option>
    <option value="therapist" selected>Терапевт</option>            
</select>
`

export default refactorTherapistSelect