const refactorDentistSelect = `
<select class="form-select form-select-default js-choose-doctor-select" aria-label=".form-select-default example" required="required">
     <option value="cardiologist">Кардиолог</option>
     <option value="dentist" selected>Стоматолог</option>
     <option value="therapist">Терапевт</option>
</select>
`

export default refactorDentistSelect