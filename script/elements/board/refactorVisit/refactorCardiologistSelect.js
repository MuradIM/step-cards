const refactorCardiologistSelect = `
<select class="form-select form-select-default js-choose-doctor-select" aria-label=".form-select-default example" required="required">
    <option value="cardiologist" selected>Кардиолог</option>
    <option value="dentist">Стоматолог</option>
    <option value="therapist">Терапевт</option>
</select>
`

export default refactorCardiologistSelect