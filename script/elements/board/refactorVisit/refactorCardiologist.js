function refactorCardiologist(obj) {
return `
    <label for="exampleInputEmail1" class="form-label mt-3">Обычное давление</label>
<input type="text" class="form-control js-info-for-object" id="pressure" aria-describedby="emailHelp" required="required" value="${obj.pressure?obj.pressure:''}">
<label for="exampleInputEmail1" class="form-label mt-3">Индекс массы тела</label>
<input type="text" class="form-control js-info-for-object" id="BMI" aria-describedby="emailHelp" required="required" value="${obj.BMI?obj.BMI:''}">
<label for="exampleInputEmail1" class="form-label mt-3">Перенесенные заболевания сердечно-сосудистой системы</label>
<input type="text" class="form-control js-info-for-object" id="pastIllness" aria-describedby="emailHelp" required="required" value="${obj.pastIllness?obj.pastIllness:''}">
<label for="exampleInputEmail1" class="form-label mt-3">Возраст</label>
<input type="text" class="form-control js-info-for-object" id="age" aria-describedby="emailHelp" required="required" value="${obj.age?obj.age:''}">
    `
}

export default refactorCardiologist