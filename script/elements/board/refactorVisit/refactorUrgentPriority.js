const refactorUrgentPriority = `
    <select class="form-select form-select-default js-choose-doctor-select mt-3" aria-label=".form-select-default example" required="required">
    <option value="usualPriority">Обычная</option>
    <option value="highPriority">Приоритетная</option>
    <option value="urgentPriority" selected>Неотложная</option>
</select>
    `

export default refactorUrgentPriority