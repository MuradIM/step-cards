function refactorVisitMutualHTML(obj) {
    return `
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Изменить данные</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form class="js-create-visit-modal-form">
          <div class="form-check form-switch js-status-wrapper">
            <label class="form-check-label" for="flexSwitchCheckDefault">Статус выполнения</label>
          </div>
          <div class="js-doctor-select"></div>
          <div class="mb-3 js-choose-doctor-input">
            <label for="exampleInputEmail1" class="form-label mt-3">Цель визита</label>
            <input type="text" class="form-control js-info-for-object" id="visitGoal" aria-describedby="emailHelp" required="required" value="${obj.visitGoal}">
            <label for="exampleInputEmail1" class="form-label mt-3">Краткое описание визита</label>
            <input type="text" class="form-control js-info-for-object" id="shortDescription" aria-describedby="emailHelp" required="required" value="${obj.shortDescription}">
            <div class="js-priority"></div>
            <label for="exampleInputEmail1" class="form-label mt-3">ФИО</label>
            <input type="text" class="form-control js-info-for-object" id="nameInfo" aria-describedby="emailHelp" required="required" value="${obj.nameInfo}">
          </div>
          <div class="mb-3 js-choose-doctor-special-input"></div>
          <button type="submit" class="btn btn-primary js-log-btn-submit-modal-create-visit mt-5">Submit</button>
        </form>
      </div>
    </div>
`
}

export default refactorVisitMutualHTML