const refactorUsualPriority= `
    <select class="form-select form-select-default js-choose-doctor-select mt-3" aria-label=".form-select-default example" required="required">
    <option value="usualPriority" selected>Обычная</option>
    <option value="highPriority">Приоритетная</option>
    <option value="urgentPriority">Неотложная</option>
</select>
    `

export default refactorUsualPriority