import refactorVisitMutualHTML from "./refactorVisitMutualHTML.js";
import refactorTherapistSelect from "./refactorTherapistSelect.js";
import refactorDentistSelect from "./refactorDentistSelect.js";
import refactorCardiologistSelect from "./refactorCardiologistSelect.js";
import refactorUsualPriority from "./refactorUsualPriority.js";
import refactorHighPriority from "./refactorHighPriority.js";
import refactorUrgentPriority from "./refactorUrgentPriority.js";
import refactorCardiologist from "./refactorCardiologist.js";
import refactorDentist from "./refactorDentist.js";
import refactorTherapist from "./refactorTherapist.js";
import StatusInput from "../../../classes/input/StatusInput.js";

function refactorVisitModal(obj) {

    const mutual = document.createElement("div")
    mutual.classList.add("modal-dialog")
    mutual.innerHTML = refactorVisitMutualHTML(obj)

    const doctorCardiologistSelect = refactorCardiologistSelect
    const doctorDentistSelect = refactorDentistSelect
    const doctorTherapistSelect = refactorTherapistSelect

    const usualPriority = refactorUsualPriority
    const highPriority = refactorHighPriority
    const urgentPriority = refactorUrgentPriority

    const cardiologistSpecial = refactorCardiologist(obj)
    const dentistSpecial = refactorDentist(obj)
    const therapistSpecial = refactorTherapist(obj)

    const placeForDoctor = mutual.querySelector(".js-doctor-select")
    const placeForPriority = mutual.querySelector(".js-priority")
    const placeForSpecialInfo = mutual.querySelector(".js-choose-doctor-special-input")

    if (obj.doctor === "cardiologist") {
        placeForDoctor.innerHTML = doctorCardiologistSelect
        placeForSpecialInfo.innerHTML = cardiologistSpecial
    } else if (obj.doctor === "dentist") {
        placeForDoctor.innerHTML = doctorDentistSelect
        placeForSpecialInfo.innerHTML = dentistSpecial
    } else if (obj.doctor === "therapist") {
        placeForDoctor.innerHTML = doctorTherapistSelect
        placeForSpecialInfo.innerHTML = therapistSpecial
    }

    if (obj.priority === "usualPriority") {
        placeForPriority.innerHTML = usualPriority
    } else if (obj.priority === "highPriority") {
        placeForPriority.innerHTML = highPriority
    } else if (obj.priority === "urgentPriority") {
        placeForPriority.innerHTML = urgentPriority
    }

    const docSelect = mutual.querySelector(".js-choose-doctor-select")
    docSelect.addEventListener("click", e => {

        if (e.target.value === "cardiologist") {
            placeForSpecialInfo.innerHTML = cardiologistSpecial
        } else if (e.target.value === "dentist") {
            placeForSpecialInfo.innerHTML = dentistSpecial
        } else if (e.target.value === "therapist") {
            placeForSpecialInfo.innerHTML = therapistSpecial
        }
    })

    const statusWrapper = mutual.querySelector(".js-status-wrapper")
    const statusInput = new StatusInput(obj.isDone)
    statusWrapper.prepend(statusInput.render())

    return mutual

}

export default refactorVisitModal