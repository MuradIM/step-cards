function dentistHTML(obj, priority) {
    return `
        <h6 class="card-title">Цель визита:</h6>
        <p class="card-text js-goal-for-search">${obj.visitGoal}</p>
        <h6 class="card-title">Краткое описание:</h6>
        <p class="card-text js-short-description-for-search">${obj.shortDescription}</p>
        <h6 class="card-title">Срочность:</h6>
        <p class="card-text">${priority}</p>
        <h6 class="card-title">Дата последнего визита:</h6>
        <p class="card-text">${obj.lastVisitDate}</p>
    `
}

export default dentistHTML