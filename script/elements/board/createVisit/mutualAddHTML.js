const mutualAddHTML = `
<label for="exampleInputEmail1" class="form-label mt-3">Цель визита</label>
<input type="text" class="form-control js-email js-info-for-object" id="visitGoal" aria-describedby="emailHelp" required="required">
<label for="exampleInputEmail1" class="form-label mt-3">Краткое описание визита</label>
<input type="text" class="form-control js-email js-info-for-object" id="shortDescription" aria-describedby="emailHelp" required="required">
<select class="form-select form-select-default js-choose-doctor-select mt-3" aria-label=".form-select-default example" required="required">
    <option selected value=''>Срочность</option>
    <option value="usualPriority">Обычная</option>
    <option value="highPriority">Приоритетная</option>
    <option value="urgentPriority">Неотложная</option>
</select>
<label for="exampleInputEmail1" class="form-label mt-3">ФИО</label>
<input type="text" class="form-control js-info-for-object" id="nameInfo" aria-describedby="emailHelp" required="required">
`

export default mutualAddHTML