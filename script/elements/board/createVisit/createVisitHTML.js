const createVisitHTML = `
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Create visit</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form class="js-create-visit-modal-form">
          <select class="form-select form-select-default js-choose-doctor-select" aria-label=".form-select-default" required="required">
            <option selected value=''>Выберите врача</option>
            <option value="cardiologist">Кардиолог</option>
            <option value="dentist">Стоматолог</option>
            <option value="therapist">Терапевт</option>
          </select>
          <div class="mb-3 js-choose-doctor-input"></div>
          <div class="mb-3 js-choose-doctor-special-input"></div>
          <button type="submit" class="btn btn-primary js-log-btn-submit-modal-create-visit mt-5">Submit</button>
        </form>
      </div>
    </div>
  </div>
`

export default createVisitHTML